import socket
import random
import secrets
import binascii
import os.path
import hashlib


def main():
    chunk_size = 10**6  # download 1MB at a time
    download_directory = os.path.expanduser('~/Downloads')
    header_secret = secrets.token_hex(4)
    header_secret_bytes = header_secret.encode()
    header_secret_length = len(header_secret_bytes)
    port = random.randrange(1000, 65535)
    listener = socket.socket()
    listener.bind(('0.0.0.0', port))
    listener.listen()
    connection = None
    print('Listening on port {}'.format(port))
    print('Header secret:', header_secret)
    try:
        while True:
            connection, info = listener.accept()
            print('Got connection from {}:{}'.format(*info))
            start = connection.recv(header_secret_length)
            if start != header_secret_bytes:
                print('Connection sent bad secret, closing.')
                connection.close()
                continue
        
            filename_len = int.from_bytes(connection.recv(2), 'big')
            filename = connection.recv(filename_len).decode()
            filename = os.path.basename(filename)
            if filename.rfind('.') > 0:   
                filename_og, filename_ext = filename.rsplit('.', 1)
                filename_ext = '.' + filename_ext
            else:
                filename_og = filename
                filename_ext = ''
            
            save_path = os.path.join(download_directory, filename)
            i = 1
            while os.path.exists(save_path):
                i += 1
                save_path = os.path.join(
                    download_directory,
                    '{} ({}){}'.format(filename_og, i, filename_ext)
                )
            filename = os.path.basename(save_path)
        
            content_digest = connection.recv(32)
            file_size = int.from_bytes(connection.recv(8), 'big')
            print('Downloading file',
                  'name={}'.format(filename),
                  'size={}'.format(file_size),
                  'digest={}'.format(binascii.b2a_hex(content_digest).decode()),
                  sep='\n\t')

            h = hashlib.sha256()
            f = open(save_path, 'wb')
            while file_size > 0:
                chunk = connection.recv(chunk_size)
                h.update(chunk)
                f.write(chunk)
                file_size -= len(chunk)
            f.close()

            got_digest = h.digest()
            if got_digest != content_digest:
                print('Digest mismatch!',
                      'Got: {}'.format(b2a_hex(got_digest).decode()),
                      'Expected: {}'.format(b2a_hex(content_digest).decode()),
                      sep='\n\t')
            else:
                print('Content digest is verified')

            print('Closing connection')
            connection.close()
    except KeyboardInterrupt:
        print('\nCaught KeyboardInterrupt, shutting down')
        listener.close()
        if connection:
            try:
                connection.close()
            except:
                pass


if __name__ == '__main__':
    main()
