import socket
import hashlib
import os.path
import os
import sys
import re

IPv4 = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")


def main():
    chunk_size = 10**6
    chunk_buff = bytearray(chunk_size)
    chunk_view = memoryview(chunk_buff)
    
    if len(sys.argv) < 5:
        print('missing arguments')
        sys.exit(1)
        
    hex_chars = b'0123456789abcdef'
    secret_hex = sys.argv[1].lower().encode()
    if any(c not in hex_chars for c in secret_hex):
        print('secret must contain only hex characters!')
        sys.exit(1)
    else:
        print('sending files with secret bytes {}'.format(secret_hex))

    ip = sys.argv[2]
    if not IPv4.match(ip) or any(not(0 <= int(x) <= 255) for x in ip.split('.')):
        print('invalid ip address given')
        sys.exit(1)

    port = int(sys.argv[3])
    if not (1000 <= port <= 65535):
        print('invalid port given')
        sys.exit(1)
    
    for path in sys.argv[4:]:
        if not os.path.isfile(path):
            print('Path {} is not a file, skipping'.format(path))
            continue

        print('Procesing file at {}'.format(path))
        filesize = os.stat(path).st_size
        filename = os.path.basename(path.encode())
        h = hashlib.sha256()
        f = open(path, 'rb')
        print('\tCalculating file hash')
        total_read_amount = 0
        while True:
            read_amount = f.readinto(chunk_buff)
            if read_amount:
                total_read_amount += read_amount
                h.update(chunk_view[:read_amount])
            else:
                break

        if total_read_amount != filesize:
            print('Error detected: file bytes read does not match os.stat')
            sys.exit(1)
        
        digest = h.digest()
        f.seek(0)

        print('\tSending file...')
        header = bytearray(secret_hex)
        header.extend(len(filename).to_bytes(2, 'big', signed=False))
        header.extend(filename)
        header.extend(digest)
        header.extend(filesize.to_bytes(8, 'big', signed=False))

        connection = socket.create_connection((ip, port))
        connection.sendall(header)
        total_read_amount = 0
        while total_read_amount < filesize:
            read_amount = f.readinto(chunk_buff)
            if read_amount:
                total_read_amount += read_amount
                connection.sendall(chunk_view[:read_amount])
            else:
                break
        if(total_read_amount < filesize):
            print('Failed sending all data')
            break
        
    sys.exit(0)


if __name__ == '__main__':
    main()
